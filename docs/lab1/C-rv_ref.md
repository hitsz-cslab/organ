# 附录C RISC-V参考资料

=== "RV32G指令列表"
    <embed src = "../assets/RV32G-Instructions-List.pdf" width="100%" height="1088">

=== "RISC-V中文手册"
    <embed src = "../assets/RISC-V-Reader-Chinese-v2p12017.pdf" width="100%" height="1088">
