# 验收与提交

!!! warning "注意 :fire:"
    &emsp;&emsp;请独立完成。**<span style="background-color:#FFFF00;">如发现雷同，雷同者均0分！</span>**

## 验收内容

&emsp;&emsp;课上检查字符串匹配的汇编程序能否正确运行：<font color = red>1.2</font>分

&emsp;&emsp;【附加题】  
&emsp;&emsp;&emsp;&emsp;课上检查字符串匹配的汇编程序是否符合要求：<font color = green>**+0.5**</font>分

## 提交要求

- 提交内容：

&emsp;&emsp;（1）C语言到汇编：预编译文件(`.i`)、汇编文件(`.s`)、目标文件(`.o`)及其反汇编文件(`.o.txt`)、可执行文件及其反汇编文件(`.txt`)：<font color = red>1.2</font>分

&emsp;&emsp;（2）汇编程序设计：字符串匹配的汇编程序(`.asm`)：<font color = red>1.2</font>分

&emsp;&emsp;实验报告（按报告模板完成）：<font color = red>2.4</font>分

&emsp;&emsp;【附加题】  
&emsp;&emsp;&emsp;&emsp;通过附加题验收的汇编程序(`.asm`)：<font color = green>**+0.2**</font>分，不通过验收不得分。  

- 提交格式：`学号_姓名.zip`

!!! info "关于Deadline :calendar:"
    &emsp;&emsp;作业提交Deadline以作业系统为准，请自行登录查看。
